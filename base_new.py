# using code from https://gitlab.com/mikem12/ProjectK
# modified by https://gitLab.com/Striker_Fox

import datetime
import time
import sys
from colorama import Fore, Back, Style


def help():
    help_answer = ("> How are you?\n"
                   "> What are your morals?\n"
                   "> What can you do?\n"
                   "> What is todays date?\n"
                   "> What time is it?\n"
                   "> What is my name?\n"
                   "> Help\n")
    return help_answer


def answer(userqStripped):  # Function answer can be called at any point

    if userqStripped == "How are you":
        A_answer = ("< I am fine. How are you?\n")  # Assigns variable
        return A_answer  # returns answer or variable

    elif userqStripped.lower().find("cant") >= 0:
        B_answer = ("< Fine!")  # Assigns variable
        return B_answer  # returns answer or variable

    elif userqStripped.lower().find("your morals") >= 0:
        C_answer = "< We are programmed to cause no harm to life forms.\n< What else can I help you with?"
        return C_answer

    elif userqStripped == "What can you do":
        D_answer = ("< Heres is what I can do!" + help() +
                    "\n< What else can I help you with?")
        return D_answer

    elif userqStripped == "What is todays date":
        now = datetime.datetime.now()  # Assigns now to date and time
        # Assigns time to variable
        E_answer = ("< Current date is" + (now.strftime(" %m-%d-%Y")
                                           ) + "\n< What else can I help you with?")
        return E_answer  # returns answer or variable

    elif userqStripped == "What time is it":
        now = datetime.datetime.now()  # Assigns now to date and time
        # Assigns time to variable
        F_answer = ("< Current time is" + (now.strftime(" %l:%M%p %z")
                                           ) + "\n< What else can I help you with?")
        return F_answer  # returns answer or variable

    elif userqStripped.lower().find("my name") >= 0:
        # Assigns time to variable
        G_answer = ("< Your name is " + username +
                    "\n< What else can I help you with?")
        return G_answer  # returns answer or variable

    elif userqStripped.lower().find("your name") >= 0:
        H_answer = ("my name is Chappi\n< What else can I help you with?")
        return H_answer

    elif userqStripped.lower().find("im fine") >= 0:
        I_answer = ("")
        return I_answer

    elif userqStripped.lower().find("change color to") >= 0:
        print("< here is your predefined choices\n< red and blue\n< white and black\n< purple and green\n< red and yellow\n< red and white")
        colorvalue = input("> ")
        J_answer = colorchange(colorvalue)
        return J_answer

    elif userqStripped.lower().find("help") >= 0:  # help command
        # \n< How are you? \n< You cant! \n< What are your morals \n< Help"
        Y_answer = ("< Heres what you can say!\n" + help() +
                    "\n< What else can I help you with?")
        return Y_answer

    else:
        Z_answer = "< ErRoR... \n< Eeerror \n< Sorry How can I help?"
        return Z_answer  # returns the answer


def colorchange(colorvalue):
    if colorvalue.lower().find("red and blue") >= 0:
        print(Fore.RED + Back.BLUE + 'Is this fine?')
        colorIn = input("> ")
        if colorIn == "No":
            print(Fore.RESET + Back.RESET + "< Ok \n< resetting.....")
        else:
            print("")

    elif colorvalue.lower().find("white and black") >= 0:
        print(Fore.WHITE + Back.BLACK + 'Is this fine ?')
        colorIn = input("> ")
        if colorIn == "No":
            print(Fore.RESET + Back.RESET + "< Ok \n< resetting.....")
        else:
            print("")

    elif colorvalue.lower().find("purple and green") >= 0:
        print(Fore.MAGENTA + Back.GREEN + 'Is this fine?')
        colorIn = input("> ")
        if colorIn == "No":
            print(Fore.RESET + Back.RESET + "< Ok \n< resetting.....")
        else:
            print("")

    elif colorvalue.lower().find("red and yellow") >= 0:
        print(Fore.RED + Back.YELLOW + 'Is this fine?')
        colorIn = input("> ")
        if colorIn == "No":
            print(Fore.RESET + Back.RESET + "< Ok \n< resetting.....")
        else:
            print("")

    elif colorvalue.lower().find("red and white") >= 0:
        print(Fore.RED + Back.WHITE + 'Is this fine?')
        colorIn = input("> ")
        if colorIn == "No":
            print(Fore.RESET + Back.RESET + "< Ok \n< resetting.....")
        else:
            print("")

    # Starting sequence, only said once
Stext = "\nStarting Python......\n"
Satext = "Executing Chappi.py......\nLoading"
Sbtext = "..........\n\n"
for char in Stext:  # starts a loop based on whats in Btext
    sys.stdout.write(char)  # prints out by each character
    sys.stdout.flush()  # Changes the pase of output
    time.sleep(0.1)  # amount it takes for it to print

for char in Satext:  # starts a loop based on whats in Btext
    sys.stdout.write(char)  # prints out by each character
    sys.stdout.flush()  # Changes the pase of output
    time.sleep(0.1)  # amount it takes for it to print

for char in Sbtext:  # starts a loop based on whats in Btext
    sys.stdout.write(char)  # prints out by each character
    sys.stdout.flush()  # Changes the pase of output
    time.sleep(0.6)  # amount it takes for it to print

# Welcome sequence
print("######## Chappi ########")
# I want this to search a database of welcoming sentences
print("< Welcome User My Name Is Chappi")
# This will search for a way to ask a users name
username = input("< What is your name? \n> ")

# Figure out how to filter names only, find only returns numbers

while len(username) == 0:
    # This will search for a way to ask a users name
    username = input("< What is your name? \n> ")

print("< Hello " + username + "! How can I help?\n")

# infinite loop starts Here

while True:

    userq = input("> ")  # Records users question!
    userqStripped = userq.replace("'", "")
    userqAnswer = userqStripped  # records users answer

    if userqStripped.lower().find("bye") >= 0:  # ends program if Good bye is typed
        print("< Please wait while program ends....")
        break

    elif userqStripped.lower().find("cant") >= 0:
        print(answer(userq))
        break

    elif userqStripped.lower() == "exit":
        print("< Good bye!")
        break

    else:
        Btext = "\n< ...\n"
        for char in Btext:  # starts a loop based on whats in Btext
            sys.stdout.write(char)  # prints out by each character
            sys.stdout.flush()  # Changes the pase of output
            time.sleep(0.4)  # amount it takes for it to print
        print(answer(userqStripped))  # calls function according to user answer
